# CANIFASHN

A game that tests your sense of street fashion.

Process flow:

1. The score you get begins with a node service scraping a random page from
2. lookbook, which contains a list of photos and their 'hypes' (likes);
2. Then all the scores get normalized, where 1000 would be given to the highest
3. hype score, all other being (hype*1000)/max_hype;
3. Finally:
    - the sign of the score is based on whether you correctly guessed that
    - the looks hype is more ('Fashn') or less ('No Fashn') than the average,
    - non-normalized hype for a given page
    - the nominal value of it is the difference between the normalized average
    - and a given photo's score

I guess I didn't bother explaining this to the user, since the initial idea was 
to compare your taste to that of the lookbook community 
(read 'objectively fashionable'). However in approach above, the score for a 
photo is determined relative to the other photos on the page, but not other 
pages. Since relativity is unavoidable with the set logic, the user must be
presented with a design that allows relative comparison. This, in turn, brake
s the current minimalist design, which was one of the main features.  

`webpack-dev-server --port $PORT --host $IP --content-base src/`  
`https://canifashn-jimothyhalpert7.c9users.io/webpack-dev-server/index.html`