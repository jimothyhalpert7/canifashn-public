// calculate median
// 2015-09-05T03:19 - i broke lookbook. Time to go home )

var express = require('express');
var app = express();
var request = require("request"),
	cheerio = require("cheerio");
var _ = require('lodash');


function randomIntInc(low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low);
}

var urls = [{
	url: 'http://lookbook.nu/',
	max: 39
}, 
{
	url: 'http://lookbook.nu/new/',
	max: 83
}, 
{
	url: 'http://lookbook.nu/hot/',
	max: 80
}, 
// {
// 	url: 'http://lookbook.nu/europe/new/',
// 	max: 84
// }, 
// {
// 	url: 'http://lookbook.nu//germany?page=',
// 	max: 84
// }, 
{
	url: 'http://lookbook.nu/top/',
	max: 88
}, 
// {
// 	url: 'http://lookbook.nu/north-america?page=',
// 	max: 84
// }, 
// {
// 	url: 'http://lookbook.nu/united-kingdom?page=',
// 	max: 84
// }, 
// {
// 	url: 'http://lookbook.nu/netherlands?page=',
// 	max: 84
// }, 
// {
// 	url: 'http://lookbook.nu/japan?page=',
// 	max: 84
// }
];

var getAfterFromTextByLook = function($, look) {
	var byline = $($(look).find('.byline'));
	var bylineText = byline.text();
    var afterFromTextStart = bylineText.indexOf('from ') 
    	+ 'from '.length;
    var afterFromText = bylineText.substr(afterFromTextStart, 
    	bylineText.length).trim();
    
    return afterFromText;
}

var findItemNotMatchingGivenItems = function(lookupArr, notThese) {
	return lookupArr.find(item => notThese.indexOf(item) == -1)
}

var getWrongPlaceObj = function(resItem, allPlaces) {
		var currentPlaces = resItem.places.map(place => place.place)
		
		var shuffledAllPlaces = _.shuffle(allPlaces);
		var place = {
			place: findItemNotMatchingGivenItems(shuffledAllPlaces, currentPlaces),
			right: false
		}
		
		return place;
}

var setWrongPlaces = function(result, allPlaces) {
	result.forEach((item, i) => {
		item.places.push(getWrongPlaceObj(item, allPlaces)) 
		item.places.push(getWrongPlaceObj(item, allPlaces))
		item.places = _.shuffle(item.places)
	})
};

var getUrl = function() {
	var oRandUrl = urls[randomIntInc(0, (urls.length - 1))];
	var url = oRandUrl.url + randomIntInc(1, oRandUrl.max > 4 ? (oRandUrl.max - 4) : oRandUrl.max);
	//url = 'http://lookbook.nu/17';

	console.log('oRandUrl: ', oRandUrl);
	console.log('url: ', url);
	
	return url
}

var scrape = function (res) {
	var url = getUrl();
	
	//var j = request.jar();
	//var cookie = request.cookie('look-list-gender=guys');
	//j.setCookie(cookie, url);
	
	//request({url: url, jar: j}, function (error, response, body) {
	request({
		url: url,
		gzip: true
	}, function(error, response, body) {
		if (!error) {
			console.timeEnd('scrape');
			console.time('scrapItems');
			var $ = cheerio.load(body);

			var result = [];
			var allPlaces = [];

			$('.look_v2').each(function(index, look) {
				var byline = $($(look).find('.byline'));
				if (byline.length) {
					var src = $($(look).find('img')[1]).attr('src');
					var afterFromText = getAfterFromTextByLook($, look);
					var href = $(look).attr('href');
					
					var item = {
						href: href,
						src: src,
						places: [{
							place: afterFromText,
							right: true
						}]
					};
					
					allPlaces.push(afterFromText);
					result.push(item);
				}
			}).get();
			console.timeEnd('scrapItems');
			
			setWrongPlaces(result, allPlaces);
			res.json(result);
		}
		else {
			console.log("We’ve encountered an error: " + error);
		}
	});	
}

console.log('!process.env.PORT', !process.env.PORT);
if (!process.env.PORT) {
	app.use(express.static('.'));
}
app.use(express.static('dist'));
app.use('/libs', express.static('libs'));

app.get('/getfashn', function(req, res) {
	console.time('scrape');
	scrape(res);

});

app.listen(process.env.PORT || 7358);