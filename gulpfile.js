var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var usemin = require('gulp-usemin');

gulp.task('js', function() {
    gulp.src('src/script.js')
        .pipe(concat('script.js'))
        .pipe(uglify({mangle: {toplevel: true}, compress: { drop_console: true }}))
        .pipe(gulp.dest('dist'));
});

gulp.task('css', function() {
    gulp.src('src/style.css')
        .pipe(minifyCss())
        .pipe(gulp.dest('dist'));
});

gulp.task('html', function() {
    gulp.src('src/index.html')
        .pipe(minifyHtml({ empty: true }))
        .pipe(gulp.dest('dist'));
});

gulp.task('usemin', function() {
    gulp.src('index.html')
        .pipe(usemin({
            css: [ rev() ],
            html: [ minifyHtml({ empty: true }) ],
            js: [ uglify(), rev() ],
            inlinejs: [ uglify() ],
            inlinecss: [ minifyCss(), 'concat' ]
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['js', 'css', 'html']);