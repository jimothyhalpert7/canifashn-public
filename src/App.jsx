import React from 'react';
import {
    render
}
from 'react-dom';
// import './style.css';
import Superagent from 'superagent';

var getFashnUrl = '/getfashn';

class App extends React.Component {
    constructor() {
        super()
        this.state = {
            looks: [{
                href: '',
                src: '',
                places: []
            }],
            curLookId: 0,
            placeholderImage: 'https://noahoconnell36.files.wordpress.com/2010/06/cup-to-the-ceiling-prank.jpg',
            score: 0,
            delta: '0'
        }

        this.toNextLook = this.toNextLook.bind(this)
    }

    getLooks() {
        return Superagent.get(getFashnUrl)
            .set('Accept', 'application/json')
            .end(function(err, res) {
                // console.log(JSON.stringify(err), JSON.stringify(res));
                var newLooks = this.state.looks.length > 1 ? this.state.looks.concat(res.body) : res.body
                this.setState({
                    looks: newLooks
                })
            }.bind(this));
    }

    componentDidMount() {
        this.serverRequest = this.getLooks();
        $('#scoreInfo').hide();
    }

    componentWillUnmount() {
        this.serverRequest.abort();
    }

    getCurrentLook() {
        return this.state.looks[this.state.curLookId];
    }

    getLook(id) {
        return this.state.looks[id]
    }
    
    getLookSrc(id){
        return this.getLook(id) ? this.getLook(id).src : null
    }
    
    getLookHref(id) {
        return this.getLook(id) ? this.getLook(id).href : null
    }
    
    toNextLook(rightPlace, e) {
        var newScore = rightPlace ? this.state.score + 1 : this.state.score - 1
        var newDelta = (rightPlace ? '+' : '-') + '1'
        this.setState({
            score: newScore,
            curLookId: this.state.curLookId + 1,
            delta: newDelta
        })
        
        $('#scoreInfo').removeClass('animated fadeOut');
        $('#scoreInfo').removeClass('lightgray');
        $('#scoreInfo').show();
        $('#scoreInfo').addClass('animated fadeInUp');
        $('#scoreInfo').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function () {
                $('#scoreInfo').removeClass('animated fadeInUp');
                $('#scoreInfo').addClass('animated fadeOut');
                $('#scoreInfo').addClass('lightgray');
            });        
        
        console.log(newScore)
        
        if (this.state.curLookId > this.state.looks.length - 4) {
            this.getLooks()
        }
    }

    render() {
        return (
            <div id="containerInnerInner">
                    <div id="header">
                        <div id="score">
                            <a id="scoreInner" target="_blank"
                                href={this.getLookHref(this.state.curLookId - 1)} >
                                {this.state.score}
                            </a>
                        </div>
                        <hr/>
                    </div>
                        <a id="imageContainer" href={this.getCurrentLook().href} target="_blank">
                            <img id="img" className="image"
                                 src={ this.getCurrentLook().src } />
                            <img className="displayNone"
                                 src={ this.getLookSrc(this.state.curLookId + 1) } />
                            <img className="displayNone"
                                 src={ this.getLookSrc(this.state.curLookId + 2) } />
                            <img className="displayNone"
                                 src={ this.getLookSrc(this.state.curLookId + 3) } />
                        </a>
                        <div id="scoreInfo">{this.state.delta}</div>
                        <div id="buttons">
                            { this.getCurrentLook().places.map( (place, i) =>
                                <div key={i} className="button" onClick={this.toNextLook.bind(this, place.right)}>
                                    {place.place}
                                </div>
                            )}
                        </div>
                </div>
        )
    }
}

render(<App/>, document.getElementById('app'));